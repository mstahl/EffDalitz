/*!                                  
 *  @file      makePlots_2DEff_adaptive.cpp  
 *  @author    Alessio Piucci                                    
 *  @brief     A macro to make 2D efficiencies, using adaptive binning.                         
 */

#include "Eff.h"
#include "commonLib.h"
#include <IOjuggler.h>

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TString.h>
#include <TFriendElement.h>
#include <TObjString.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
namespace pt = boost::property_tree;


int main(int argc, char** argv){
  
  //---------------------//
  //  parse job options  //
  //---------------------//
  
  std::string inFileName_num = "";
  std::string inFileName_den = "";
  std::string outFileName = "";
  std::string configFileName = "";
  unsigned int min_events = 0;
  
  extern char* optarg;
  extern int optind;
  int ca;
  
  //parse the input options
  while ((ca = getopt(argc, argv, "i:o:c:b:d:h")) != -1){
    switch (ca){

    case 'i':
      inFileName_num = optarg;
      break;

    case 'd':
      inFileName_den = optarg;
      break;
      
    case 'o':
      outFileName = optarg;
      break;
      
    case 'c':
      configFileName = optarg;
      break;
      
    case 'b':
      min_events = std::stoul(optarg);
      break;

    /*
    //to handle optional arguments
    case ':':
      switch (optopt){
	case 'd':
	  inFileName_den = "";
	  break;
      }
    */
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-d : OPTIONAL: input file name with all events (denominator)."
                << " If not specified, is taken equal to the -i argument" << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << "-b : mininum number of events per bin." << std::endl;
      std::cout << "nonoptions: any number of <file:friendtree> combinations" << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
    
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o:c:h")) != -1)

  std::vector<TString> extraopts;
  
  while (optind < argc)
    //use emplace_back to call TString ctor
    extraopts.emplace_back(argv[optind++]);
  
  //for the denominator: if it was not parsed from command line,
  //use the same TFile of the denominator
  if(inFileName_den == "")
    inFileName_den = inFileName_num;
  
  //print the parsed options
  std::cout << "inFileName_num = " << inFileName_num << std::endl;
  std::cout << "inFileName_den = " << inFileName_den << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  std::cout << "configFileName = " << configFileName << std::endl;
  std::cout << "min_events = " << min_events << std::endl;
  
  if(!extraopts.empty())
    for(const auto& opt : extraopts)
      std::cout << "friendfile:friendtree = " << opt.Data() << std::endl;
  
  //check of the parsed options
  if((configFileName == "") || (inFileName_num == "")
     || (outFileName == "") || (min_events == 0)){
    std::cout << "Error: input configuration not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }  
  
  //configuration Boost tree
  pt::ptree configtree;
  
  //parse the INFO into the property tree
  pt::read_info(configFileName, configtree);
  IOjuggler::auto_append_in_ptree(configtree);
  
  //open the input file
  TFile* inFile_num = TFile::Open(ParseEnvName(inFileName_num).c_str(), "READ");
  
  if(inFile_num == nullptr){
    std::cout << "Error: the input file of the numerator does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //open the input tree
  TTree* inTree_num = (TTree*) inFile_num->Get((configtree.get<std::string>("numerator.inTree_name")).c_str());
  
  if(inTree_num == nullptr){
    std::cout << "Error: the input tree of the numerator does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }

  std::cout << "inFileName_num = " << inFileName_num
            << ", inTreeName_num = " << configtree.get<std::string>("numerator.inTree_name") << std::endl;

  //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
  //and because they will go out of scope, we have to push them into a vector
  std::vector< TFriendElement* > fes;
  
  if(!extraopts.empty())
    for(const auto& opt : extraopts){
      auto ff = TFile::Open(ParseEnvName(static_cast<std::string>(static_cast<TObjString*>((opt.Tokenize(":")->At(0)))->String().Data())).c_str());
      fes.push_back(inTree_num->AddFriend(static_cast<TObjString*>((opt.Tokenize(":")->At(1)))->String().Data(),ff));
    }
  
  ////////
  ////////
  
  //create an Eff object
  Eff* EffPlot = new Eff(configtree, inFileName_num, outFileName);
  
  //set the minimum number of events per bin
  EffPlot->SetMinEvents(min_events);
  
  TH2D* h_2D_num = new TH2D();
  TH2D* h_2D_den = new TH2D();
  
  //variables used for the normalization of histograms
  unsigned int num_den_events = 0;
  unsigned int num_num_events = 0;
  
  //compute the 2D plot for the numerator
  h_2D_num = EffPlot->Make2DPlot(inTree_num, false, num_num_events);
  
  //open the input file with the denominator
  TFile* inFile_den = TFile::Open(inFileName_den.c_str(),"READ");
  
  if(inFile_den == nullptr){
    std::cout << "Error: the input file of the denominator does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //open the input tree with the denominator
  TTree* inTree_den = (TTree*) inFile_den->Get((configtree.get<std::string>("denominator.inTree_name")).c_str());
  
  if(inTree_den == nullptr){
    std::cout << "Error: the input tree of the denominator does not exist." << std::endl;
    exit(EXIT_FAILURE);  
  }

  std::cout << "inFileName_den = " << inFileName_den
            << ", inTreeName_den = " << configtree.get<std::string>("denominator.inTree_name") << std::endl;
    
  //compute the 2D plot for the denominator
  h_2D_den = EffPlot->Make2DPlot(inTree_den, true, num_den_events);
  
  EffPlot->Compute2DEfficiency_adaptive(h_2D_den, h_2D_num);
  
  //write the output files
  EffPlot->Finalize();
  
  return 0;
}
