/*!                                  
 *  @file      makePlots_2DEff_adaptive_filtered.cpp  
 *  @author    Alessio Piucci                                    
 *  @brief     A macro to make 2D efficiencies, using adaptive binning using filtered MC samples.                         
 */

#include "Eff.h"
#include "commonLib.h"
#include <IOjuggler.h>

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TString.h>
#include <TFriendElement.h>
#include <TObjString.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
namespace pt = boost::property_tree;


int main(int argc, char** argv){
  
  //---------------------//
  //  parse job options  //
  //---------------------//
  
  std::string inFileName = "";
  std::string outFileName = "";
  std::string configFileName = "";
  unsigned int min_events = 0;
  
  extern char* optarg;
  extern int optind;
  int ca;
  
  //parse the input options
  while ((ca = getopt(argc, argv, "i:o:c:b:h")) != -1){
    switch (ca){

    case 'i':
      inFileName = optarg;
      break;
      
    case 'o':
      outFileName = optarg;
      break;
      
    case 'c':
      configFileName = optarg;
      break;
      
    case 'b':
      min_events = std::stoul(optarg);
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << "-b : mininum number of events per bin." << std::endl;
      std::cout << "nonoptions: any number of <file:friendtree> combinations" << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
    
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o:c:h")) != -1)

  std::vector<TString> extraopts;
  
  while (optind < argc)
    //use emplace_back to call TString ctor
    extraopts.emplace_back(argv[optind++]);
  
  //print the parsed options
  std::cout << "inFileName = " << inFileName << std::endl;
  std::cout << "outFileName = " << outFileName << std::endl;
  std::cout << "configFileName = " << configFileName << std::endl;
  std::cout << "min_events = " << min_events << std::endl;
  
  if(!extraopts.empty())
    for(const auto& opt : extraopts)
      std::cout << "friendfile:friendtree = " << opt.Data() << std::endl;
  
  //check of the parsed options
  if((configFileName == "") || (inFileName == "")
     || (outFileName == "") || (min_events == 0)){
    std::cout << "Error: input configuration not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }  
  
  //configuration Boost tree
  pt::ptree configtree;
  
  //parse the INFO into the property tree
  pt::read_info(configFileName, configtree);
  IOjuggler::auto_append_in_ptree(configtree);
  
  //open the input file
  TFile* inFile = TFile::Open(ParseEnvName(inFileName).c_str(), "READ");
  
  if(inFile == nullptr){
    std::cout << "Error: input file does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //open the input tree
  TTree* inTree = (TTree*) inFile->Get((configtree.get<std::string>("numerator.inTree_name")).c_str());
  
  if(inTree == nullptr){
    std::cout << "Error: input tree does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }

  std::cout << "inFileName = " << inFileName
            << ", inTree = " << configtree.get<std::string>("numerator.inTree_name") << std::endl;

  //add friend tree(s). for this we have to fiddle apart file and friendtree in the nonoptions, i.e. <file:friendtree>
  //and because they will go out of scope, we have to push them into a vector
  std::vector< TFriendElement* > fes;
  if(!extraopts.empty())
    for(const auto& opt : extraopts){
      auto ff = TFile::Open(ParseEnvName(static_cast<std::string>(static_cast<TObjString*>((opt.Tokenize(":")->At(0)))->String().Data())).c_str());
      fes.push_back(inTree->AddFriend(static_cast<TObjString*>((opt.Tokenize(":")->At(1)))->String().Data(),ff));
    }
  
  ////////
  
  //create an Eff object
  Eff* EffPlot = new Eff(configtree, inFileName, outFileName);
  
  //set the minimim number of events per bin
  EffPlot->SetMinEvents(min_events);

  //variables used for the normalization of histograms
  unsigned int num_gen_events = 0;
  unsigned int num_reco_events = 0;
  
  //compute the 2D plot for reconstructed events
  TH2D* h_2D_reco = new TH2D();
  h_2D_reco = EffPlot->Make2DPlot(inTree, false, num_reco_events);

  for(int i_bin = 0; i_bin < h_2D_reco->GetNcells(); ++i_bin)
    std::cout << h_2D_reco->GetBinContent(i_bin) << std::endl;
    
  //get the average efficiency of the filtered sample
  //<value, error>
  std::pair<double, double> filtering_efficiency = std::make_pair(configtree.get<double>("filtering_efficiency"),
                                                                  configtree.get<double>("filtering_efficiency_err"));
  
  //print out the filtering efficiency
  std::cout << "filtering_efficiency = " << filtering_efficiency.first
            << " +- " << filtering_efficiency.second << std::endl;
  
  //make the 2D plot for the generated events:
  //uniform population in the bins, taking into account the average filtering efficiency
  //take the ranges from the reconstructed histo
  TH2D* h_2D_gen = new TH2D();
  h_2D_gen = (TH2D*) h_2D_reco->Clone();
  
  //clean the content
  h_2D_gen->Clear();
  h_2D_gen->Sumw2();
  
  //the total generated population is gen population = reco population / average efficiency
  //then, the per-bin population is = gen_population / num_bins
  num_gen_events = h_2D_reco->GetEntries() / filtering_efficiency.first;
  
  double bin_content = num_gen_events / h_2D_gen->GetNcells();
  
  //now fill the generated histo!
  for(int i_bin = 0; i_bin < h_2D_gen->GetNcells(); ++i_bin){
    h_2D_gen->SetBinContent(i_bin, bin_content);
    
    //what about the error?
    //h_2D_gen->SetBinContentError(i_bin, ???);
  }

  for(int i_bin = 0; i_bin < h_2D_gen->GetNcells(); ++i_bin)
    std::cout << h_2D_reco->GetBinContent(i_bin) << ", " << h_2D_gen->GetBinContent(i_bin) << std::endl;
  
  //compute the efficiency
  EffPlot->Compute2DEfficiency_adaptive(h_2D_gen, h_2D_reco);
  
  //write the output files
  EffPlot->Finalize();
  
  return 0;

}
