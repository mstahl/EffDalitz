/*!
 *  @file      makeBDTEff.cpp
 *  @author    Alessio Piucci
 *  @date      12-09-2016
 *  @brief     Script to make efficiency plots for the BDT cuts
 *  @return    Create an output root file containing the efficiency plots
 */

//from std
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <unistd.h>  //to use getopt in the parser 
#include <fstream>   //to write the log in an external stream

#include "../include/commonLib.h"
#include "../2DAdaptiveBinning/include/TH2A.h"

//from BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string.hpp>

//from ROOT
#include <TROOT.h>
#include "TFile.h"
#include "TH2D.h"
#include "TH3D.h"

using namespace std;
namespace pt = boost::property_tree;

//To create efficiency plots with TEfficiency
void makeTEfficiency(TH2D *h_BDT_passed, TH2D *h_BDT_total, TFile *outFile, std::string outHisto_name){
  
  TH2D* h_BDT_errors_pass;
  TH2D* h_BDT_errors_total;
  
  //the TEfficiency doesn't like not empty underflow and overflow bins
  h_BDT_total->ClearUnderflowAndOverflow();
  h_BDT_passed->ClearUnderflowAndOverflow();
  
  //to use the same axis ranges for the error histo, I clone the original histo and then reset its content
  h_BDT_errors_pass = (TH2D*) h_BDT_passed->Clone((outHisto_name + "_errors_pass").c_str());
  h_BDT_errors_pass->Reset();
  
  h_BDT_errors_total = (TH2D*) h_BDT_total->Clone((outHisto_name + "_errors_total").c_str());
  h_BDT_errors_total->Reset();
  
  //counter used for debug
  unsigned int num_fixed_bins = 0;
  
  //check consistency of generated and reco histograms
  if(!TEfficiency::CheckConsistency(*h_BDT_passed, *h_BDT_total)){
    std::cout << "Warning: passed and total BDT histos are not consistent! Trying to fix them." << std::endl;
    
    FixTEfficiencyEntries(h_BDT_passed, h_BDT_total, h_BDT_errors_pass, h_BDT_errors_total, num_fixed_bins);
    FixTEfficiencyWeights(h_BDT_passed, h_BDT_total);
    //exit(EXIT_FAILURE);
  }
  
  std::cout << "num_fixed_bins for TEfficiency = " << num_fixed_bins
            << " (" << (num_fixed_bins/(double) h_BDT_passed->GetNcells())*100 << "%)" << std::endl;
  
  TEfficiency *h_BDT_eff = new TEfficiency(*h_BDT_passed, *h_BDT_total);
  h_BDT_eff->SetName((outHisto_name + "_eff").c_str());
  
  //use Wilson statistic, please don't ask me but use this reference:
  //https://root.cern.ch/doc/master/classTEfficiency.html
  h_BDT_eff->SetConfidenceLevel(0.683);
  h_BDT_eff->SetStatisticOption(TEfficiency::kFWilson);
  
  //write the out histos
  outFile->cd();
  
  h_BDT_eff->Write(h_BDT_eff->GetName());
  h_BDT_errors_pass->Write(h_BDT_errors_pass->GetName());
  h_BDT_errors_total->Write(h_BDT_errors_total->GetName());
  
  return;
}

//To create efficiency plots with adaptive binning
void makeAdaptive(TH2D *h_BDT_passed, TH2D *h_BDT_total, TFile *outFile,
                  std::string outHisto_name, unsigned int min_events){
  
  TH2A h_gen_adaptive;
  TH2A h_reco_adaptive;
  
  //for debug
  //h_gen_adaptive.SetVerbosity(3);
  //h_reco_adaptive.SetVerbosity(3);
  
  //fill the adaptive binning histos
  h_gen_adaptive.MakeBinning(std::vector<TH2D>{*h_BDT_total, *h_BDT_passed}, min_events);
  h_reco_adaptive = h_gen_adaptive;
  h_reco_adaptive.FillFromHist(*h_BDT_passed);
  
  TH2A *h_BDT_eff = new TH2A();
  h_BDT_eff->SetName((outHisto_name + "_eff").c_str());
  
  unsigned int num_fixed_bins = 0;
  
  //check the consistency of the reco and gen histos
  CheckConsistency_adaptive(&h_gen_adaptive, &h_reco_adaptive, num_fixed_bins, false);

  std::cout << "num_bins = " << h_gen_adaptive.GetNBins() << std::endl;
  std::cout << "num_fixed_bins = " << num_fixed_bins
            << " (" << (((double) num_fixed_bins)/h_gen_adaptive.GetNBins())*100.  << "%)" << std::endl;
  
  //use Wilson statistic, please don't ask me but use this reference:
  //https://root.cern.ch/doc/master/classTEfficiency.html
  h_BDT_eff->SetStatOpt(TEfficiency::kFWilson);
  int status_adaptive = h_BDT_eff->BinomialDivide(h_reco_adaptive, h_gen_adaptive);
  
  std::cout << "status_adaptive = " << status_adaptive << std::endl;
  
  //write the out histo
  outFile->cd();
  
  h_BDT_eff->Write(h_BDT_eff->GetName());
  
  return;
  
}

int main(int argc, char** argv){
  
  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string inFile_name = "";
  std::string outFile_name = "";
  std::string configFile_name = "";
  unsigned int adaptive = 1;
  unsigned int switch_axis = 0;
  unsigned int min_events = 0;
  
  extern char* optarg;
  
  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "i:o:c:a:r:b:h")) != -1){
    switch (ca){
      
    case 'i':
      inFile_name = optarg;
      break;
      
    case 'o':
      outFile_name = optarg;
      break;
      
    case 'c':
      configFile_name = optarg;
      break;
      
    case 'a':
      adaptive = stoul(optarg);
      break;
      
    case 'r':
      switch_axis = std::stoul(optarg);
      break;
      
    case 'b':
      min_events = std::stoul(optarg);
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << "-a : 1 or 0, to use the adaptive binning (1) or not (0)." << std::endl;
      std::cout << "-r : 1 or 0, to reverse the x and y axis (1) or not (0)." << std::endl;
      std::cout << "-b : mininum number of events per bin, used by adaptive binning only." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;

    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:c:o")) != -1)  
  
  //check of the acquired options
  if((configFile_name == "") || (inFile_name == "") || (outFile_name == "") || (min_events == 0)
     || ((adaptive != 0) && (adaptive != 1)) || ((switch_axis != 0) && (switch_axis != 1))){
    std::cout << "Error: input configuration not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //convert env variables, if needed
  inFile_name = ParseEnvName(inFile_name);
  outFile_name = ParseEnvName(outFile_name);
  configFile_name = ParseEnvName(configFile_name);
  
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;
  std::cout << "configFile_name = " << configFile_name << std::endl;
  std::cout << "adaptive = " << adaptive << std::endl;
  std::cout << "switch_axis = " << switch_axis << std::endl;
  std::cout << "min_events = " << min_events << std::endl;
  
  //configuration Boost tree
  pt::ptree configtree;

  //parse the INFO into the property tree
  pt::read_info(configFile_name, configtree);
  
  std::string inHisto_name = configtree.get<std::string>("inHisto_name");
  std::string outHisto_name = configtree.get<std::string>("outHisto_name");
  double cut = configtree.get<double>("bdtcut");
  
  std::cout << "inHisto_name = " << inHisto_name << std::endl;
  std::cout << "outHisto_name = " << outHisto_name << std::endl;
  std::cout << "cut = " << cut << std::endl;
  
  //open the output stream for the log, and write the job options
  std::ofstream log_stream(outFile_name + "_log.log", ios::out);
  
  log_stream << "inFile_name = " << inFile_name << std::endl;
  log_stream << "outFile_name = " << outFile_name << std::endl;
  log_stream << "configFile_name = " << configFile_name << std::endl;
  log_stream << "adaptive = " << adaptive << std::endl;
  log_stream << "cut = " << cut << std::endl;
  
  //open the input file with the histos
  TFile *inFile = TFile::Open(inFile_name.c_str(), "READ");
  
  if(inFile == NULL){
    std::cout << "Input file " << inFile_name << " not found." << std::endl;
    exit(EXIT_FAILURE);  
  }
  
  //open histo with efficiency
  TH3D *histo_BDT = (TH3D*) inFile->Get(inHisto_name.c_str());
  
  if(histo_BDT == NULL){
    std::cout << "Input histo " << inHisto_name << " not found." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //now I need to create two TH2D to fill the TEfficiency or adaptive stuff,
  //one with all events, the other with passed ones
  
  //some debug histos
  TH3D* h_BDT_errors_3D;
  TH2D* h_BDT_errors_3D_to2D;
  
  //to use the same axis ranges for the error histo, I clone the original histo and then reset its content
  h_BDT_errors_3D = (TH3D*) histo_BDT->Clone((outHisto_name + "_errors_3D").c_str());
  h_BDT_errors_3D->Reset();

  //counter used for debug
  unsigned int num_negative_bins = 0;
  
  //I first search and fix for negative bins
  FixNegativeBins(histo_BDT, h_BDT_errors_3D, num_negative_bins);
  
  std::cout << "num_negative_bins = " << num_negative_bins
            << " (" << (num_negative_bins/(double) histo_BDT->GetNcells())*100 << " %)" << std::endl;
  
  log_stream << "num_negative_bins = " << num_negative_bins
             << " (" << (num_negative_bins/(double) histo_BDT->GetNcells())*100 << " %)" << std::endl;
  
  //Project3D rocks!!
  if(switch_axis == 0)
    h_BDT_errors_3D_to2D = (TH2D*) h_BDT_errors_3D->Project3D("yx_a");
  else
    h_BDT_errors_3D_to2D = (TH2D*) h_BDT_errors_3D->Project3D("xy_a");
  
  h_BDT_errors_3D_to2D->SetName((outHisto_name + "_errors_3D_to2D").c_str());
  
  TH2D *h_BDT_total;
  
  if(switch_axis == 0)
    h_BDT_total = (TH2D*) histo_BDT->Project3D("yx_1");
  else
    h_BDT_total = (TH2D*) histo_BDT->Project3D("xy_1");
  
  //to select only passed events, I set the range of the TH3D histo,
  //and then I project it again
  histo_BDT->GetZaxis()->SetRangeUser(cut, histo_BDT->GetZaxis()->GetXmax());
  
  TH2D *h_BDT_passed;
  
  if(switch_axis == 0)
    h_BDT_passed = (TH2D*) histo_BDT->Project3D("yx_2");
  else
    h_BDT_passed = (TH2D*) histo_BDT->Project3D("xy_2");
  
  //open the output file
  TFile *outFile = TFile::Open((outFile_name + ".root").c_str(), "recreate");
  
  //now fill TEfficiency or adaptive stuff?
  if(adaptive == 1)
    makeAdaptive(h_BDT_passed, h_BDT_total, outFile, outHisto_name, min_events);
  else
    makeTEfficiency(h_BDT_passed, h_BDT_total, outFile, outHisto_name);
  
  //write the simple average efficiency in the log stream
  log_stream << "simple mean efficiency = " << ((double) h_BDT_passed->GetEntries())/ ((double) h_BDT_total->GetEntries())
             << " +- " << BinomialError(h_BDT_passed->GetEntries(), h_BDT_total->GetEntries()) << std::endl;
    
  //write some debug histos
  outFile->cd();
  h_BDT_errors_3D->Write(h_BDT_errors_3D->GetName());
  h_BDT_errors_3D_to2D->Write(h_BDT_errors_3D_to2D->GetName());
  
  //close the files
  inFile->Close();
  outFile->Close();

  //close and write the output log
  log_stream.close();
  
  return 0;
  
}
