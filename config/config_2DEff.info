; 12/07/2016 - Alessio Piucci
; Example option file for the makePlots_2DEff macro

; Numerator options
numerator {
      ; input tree
      inTree_name           "DecayTree_fixed_PIDresampled_renamed"
      
      ; cuts to apply to the event
      ; they are applied on top of the denominator cuts
      cuts		    "(0. <= Lb_BKGCAT) && (Lb_BKGCAT <= 20.)"
            
      ; kinematic variables to use for the Dalitz plot
      ; name, number of bins, lower and higher boundaries
      var1_name	  	    "m2_LcD0_TRUE"
      var1_numbins	    5000
      var1_low		    16000000.
      var1_high		    27000000.

      var2_name             "m2_D0K_TRUE"
      var2_numbins	    5000
      var2_low		    5000000.
      var2_high		    12000000.
            
      ; options of the output 2D plot
      out_name	       	    "h_LbEff_Dalitz"
      title		    "m2(LcD0) vs m2(D0K)"
      x_axis_label	    "m2(LcD0)"
      y_axis_label	    "m2(D0K)"
      
      ; titles of the plots of the 1D distributions 
      var1_title      	    "m2_LcD0"
      var2_title	    "m2_D0K"
}

; Denominator options
denominator {

      ; input file
      inTree_name           "MCDecayTree_fixed_2"
      
      ; cuts to apply to the denominator
      ; they will be automatically applied to the numerator as well
      cuts		    ""

      ; name of the variables
      var1_name            "m2_LcD0_TRUE"
      var2_name            "m2_D0K_TRUE"

      ; plot title
      title                 "Efficiency over the m2(LcD0) vs m2(D0K) Dalitz plot"
}

; Optional options to normalise the numerator and/or the denominator by some constant factor
; If you don't need them, then you can just comment/delete them
;normalise {
;      NormalizeNum          0
;      NormalizeNum_inFile   ""
;      
;      NormalizeDen          0
;      NormalizeDen_inFile   ""
;}
      
; Optional options to reweight the Dalitz plot
; If you don't need them, then you can just comment/delete them
;reweight {
;      fromTree {
;            ; reweight taking weights from a (friend) tree
;            reweightFromTree   1       ; turn on or off
;            useFriendTree      1       ; take weights from external tree by adding it as friend tree?
;            friendFileGen      "fileWithGenWeights.root"
;            friendTreeGen      "treename"
;            weightNameGen      "weightname"
;            friendFileReco     "fileWithRecoWeights"
;            friendTreeReco     "treename"
;            weightNameReco     "weightname"
;      }
;
;      2DReweight {
;            reweight      0
;            file_name     ""
;            name          ""
;            x_var         ""
;            y_var         ""
;      }
;}
