
# The EffDalitz package

The package consists of many scripts to compute efficiencies and correct data by efficiency, in an event-per-event base.
The scripts are placed in the [src](src/) directory of this repository,
and they all make use of the [TH2A adaptive binning](https://gitlab.cern.ch/mstahl/2DAdaptiveBinning)
(a few of them are also compatible with TEfficiency objects, but this feature is not maintained anymore).
The scripts are:
- `make2DEff_adaptive`: to compute efficiency maps over 2D distributions with adaptive binning, optionally reweighting some 2D input distributions;
- `make2DEff_adaptive_filtered`: like for make2DEff_adaptive, but specific for MC filtered samples (where the generated particles are not accessible anymore);
- `make2DEff`: to compute efficiency maps over 2D distributions with TEfficiency objects, optionally reweighting some 2D input distributions;
- `make1DEff`: to compute efficiency maps over 1D distributions with TEfficiency objects;
- `makePIDEff`: to compute a 3-dimensional PID efficiency map with adaptive binning,
from the output of the [PIDCalib](https://twiki.cern.ch/twiki/bin/view/LHCb/PIDCalibPackage) package;
- `yield`: to correct s-fitted data by efficiencies in an event-per-event base;
this script is not maintained anymore, since the efficiency look-up tables of the BDTs are created within DfromBBDT already;
- `mean2DEff`: to compute the mean efficiency over a 2-dimensional distribution.

Finally there are some other scripts which are not maintained anymore, and they might be removed at some point:
- `makeBDTEff`: to compute the BDT efficiency maps for the BDTs trained by the [DfromBBDT](https://gitlab.cern.ch/sneubert/DfromBBDTs) project;
- `combPlots`: to combine 1D or 2D input histograms, using the basic mathematical operations of sum, difference, multiplication and division;
this is useful if you need to combine efficiency distributions.

# Scripts
All scripts can be launched by shell terminal, providing some options that will be internally parsed.
The most common options to be parsed are the input and output file names, and the config file name.
You can get the full list of options calling a specific script with the `-h` option.
In the [Snakefile](Snakefile) you can find some example rules, that are more generally described below.

## make2DEff_adaptive
This script produces a 2-dimensional efficiency map using the adaptive binning.
It takes two trees as input (numerator and denominator trees),
and it computes the efficiency as ratio between the numerator/denominator distributions. 
In the output file you will have the TH2A objects, and the pure-TH2D objects as well to better visualise the distributions.
You can	apply cuts to your numerator/denominator specifying them in the	config file.

You can launch the script by:

```
build/bin/make2DEff_adaptive -i <input_file_name> -c <config_file_name> \
  -b <min_events_perbin> -o <output_file_name>
```

where the minimum number of events per bin is the one used to define the binning in the adaptive binning framework:
it is the minimum number of events required to populate each of the bins of the numerator histogram.

Have in	mind that the output file name has to be specified without .root extension; it will be added internally.

By default the script assumes that the numerator and denominator trees are placed in the same input file.
If they are in different files, you can specify the numerator file through
the `-i <numerator_file>` argument, and the denominator with the `-d <denominator_file>` argument.

## make2DEff
This script produces a 2-dimensional efficiency map using TEfficiency.
It takes two trees as input (numerator and denominator trees),
and it computes the efficiency as ratio	between	the numerator/denominator distributions.
In the output file you will have the TEfficiency objects, and the pure-TH2D objects as well to better visualise the distributions.
You can apply cuts to your numerator/denominator specifying them in the config file.

You can launch the script by:

```
build/bin/make2DEff -i <input_file_name> -c <config_file_name> \
  -b <min_events_perbin> -o <output_file_name>
```

Have in mind that the output file name has to be specified without .root extension; it will be added internally.

By default the script assumes that the numerator and denominator trees are placed in the same input file.
If they are in different files, you can specify the numerator file through
the `-i <numerator_file>` argument, and the denominator with the `-d <denominator_file>` argument.

## make1DEff
This script produces a 1-dimensional efficiency map using TEfficiency.
It takes two trees as input (numerator and denominator trees),
and it computes the efficiency as ratio between the numerator/denominator distributions.
In the output file you will have the TEfficiency objects, and the pure-TH1D objects as well to better visualise the distributions.
You can	apply cuts to your numerator/denominator specifying them in the	config file.

You can launch the script by:
```
build/bin/make1DEff -n <numerato_file_name> -l <numerator_tree_name> \
  -d <denominator_file_name> -m <denominator_tree_name> \
  -c <config_file_name> -o <output_file_name>
```

## make2DEff_adaptive_filtered
This script produces a 2-dimensional efficiency map using the adaptive binning,
and it is specifically written to be used with filtered-MC sample, which have no generated information available
(i.e. the denominator tree is missing).
It takes as input the tree of the reconstructed candidates (the numerator),
and the average filtering efficiency.
Assuming that the MC distributions where generated uniformly, then the efficiency shape is computed as:

`
eff(x, y) = reco(x, y) / filtering_eff
`

You can apply cuts to your numerator specifying them in the config file.

You can launch the script by:

```
build/bin/make2DEff_adaptive_filtered -i <input_file_name> -c <config_file_name> \
  -b <min_events_perbin> -o <output_file_name>
```

where the minimum number of events per bin is the one used to define the binning in the adaptive binning framework:
it is the minimum number of events required to populate each of the bins of the numerator histogram.

Have in mind that the output file name has to be specified without .root extension; it will be added internally.

## makePIDEff
This script assumes you have the output of PIDCalib binned in 3 dimensions (usually momentum, rapidity, number of tracks in the event).
Since the TH2A is compatible with 2-dimensional efficiency maps only,
the script creates a 2D map for each of the third dimension used in PIDCalib; in particular, it assumes that there are 5 bins in this dimension.
This will result in 5 2D efficiency maps in your output file.

```
build/bin/makePIDEff -i <input_file_name> -o <output_file_name> \
  -p <passed_histo_name> -t <total_histo_name> \
  -a 1 -r 0 -1 <min_events_bin1> -2 <min_events_bin2> -3 <min_events_bin3> -4 <min_events_bin4> -5 <min_events_bin5>
```

where you have to specify the minimum number of events, for each of the 5 efficiency maps.
Have in mind that the output file name has to be specified without .root extension; it will be added internally.
The `-a` option can be used to select the adaptive binning algorithm (== 1) or the TEfficiency (== 0).
The `-r` option is used to switch the x and y axis with respect the input PIDCalib maps.

## mean2DEff
This script produces an `.info` output file with the mean value of a 2-dimensional efficiency distribution.
You can launch it by:

```
build/bin/mean2DEff -i <input_file_name> -c <config_file_name> -o <output_file_name>
```

## yield
It takes as input a s-fitted dataset (in TTree format), and it computes the efficiency-corrected yield as:

```
yield = \sum_i sweight_i / eff_i
```

where the `i` index is running over the events of the dataset, and `eff_i` is the total efficiency of the event.
The macro can be run by:

```
build/bin/yield -i <input_file_name> -t <input_tree_name> -c <config_file_name> -o <output_file_name>
```

Have in mind that the output file name has to be specified without .root extension; it will be added internally.

# Option files

All the scripts of the EffDalitz package are configured by config files.
Some examples how to configure them are provided in the [config/](config) directory, and are described in the following paragraphes.

## make2DEff_adaptive
[This is](config/config_2DEff.info) an example config file.
The config file is split in numerator and denominator options, to configure them indipendently.
For the numerator, you have to specify:
- the input tree name;
- some cuts that you may want to apply to select a sub-sample of your tree; keep empty if you don't want to apply any cut;
these cuts are applied on top of the denominator cuts, i.e. the cuts specified for the denominator will be applied anyhow to the numerator,
independently if this field is kept empty or not;
- variable name, number of bins, and range for both the two variables defining the 2D distribution of your numerator;
please have in mind that to correctly use the adaptive binning you have to set an iper-fine binning (5k bins, in the example);
- some additional options for the output histogram.

The denominator is configured similarly, and it needs:
- the input tree name;
- some cuts that you may want to apply to select a sub-sample of your tree; keep empty if you don't want to apply any cut;
be aware that these cuts will be also applied to the numerator;
- names of the variables on which the 2D distribution of the denominator will be binned;
the binning of the denominator will be the same used for the numerator.

You can optionally configure the script to normalise the numerator and/or the denominator by some constant factors,
or to reweight your distributions. They are commented in the example config file.

## make2DEff
It uses the same config files of the `make2DEff_adaptive` script.

## make2DEff_adaptive_filtered
[This is](config/config_2DEff_filtered.info) an example config file.
The numerator options are configured in the same way as for the `make2DEff_adaptive` script-
Furthermore, you have to specify the average filtering efficiency and its uncertainty.

Since the framework expects the denominator cuts to be propagated to the numerator,
you have to specify them (with an empty string, for instance), even if there is no denominator in this case.
This feature will be fixed at some point...

## make1DEff
[This is](config/config_1DEff.info) an example config file.
In the config file you have to specify the binning options (number of bins, label and range of the axis),
the variables name (for both numerator and denominator), the cuts to apply to the numerator and denominator,
the names for the output histograms that will be created.

## mean2DEff
It uses the same config files of the `make2DEff(_adaptive)` scripts.

## yield
[Here](config/effLUT.info) you can find an example config file.
There you have to specify the options for the kinematic, BDT and PID efficiencies.
The efficiency correction is performed on a year-base: for each year,
you have to provide the respective files containing the all the efficiency maps that you want to use.
Be aware that you must have a 'year' variable in your input tree with the data events to be corrected.
Also, you must have in your dataset a variable which contains the s-weight used for yield computation;
set the name of that variable through the `sweights_name` field.

You first have to enable/disable some specific year that you want to handle, acting on the `status_years` boolean flags.

The correction by the kinematic efficiency is done using one single efficiency map (per year), which includes all the single kinematic contributions.
Id est, the map to be specified should include everything from the trigger selection to the analysis (kinematic) selection.
For the kinematic efficiency, you have to set:
- the names of the kinematic variables as they are specified in the dataset that you want to correct,
used to retrieve the correct phase space point in the 2D efficiency distribution;
- for each different year: the names of the files and histos containing the efficiency distributions
for the fiducial, generator and trigger->sel contributions, independently.
Optionally, you can also provide the single efficiency contributions (trigger, reconstruction + stripping, analysis selection)
in order to be used for monitoring purposes.

Optionally you can correct your dataset by the BDT efficiency, setting any number of cuts you need, on the same or on different particles.
For each of the BDT cuts you have to specify:
- `part_name`: particle name on which the BDT is applied, as it appears in the dataset that you want to correct;
- `varname_1`, varname_2`: names of the variables on which the BDT efficiency maps are binned;
- for each year, the input file name containing the BDT efficiency map.

Optionally you can also correct by the PID efficiency, setting any number of PID cuts you want, on different particles.
Currently, the effLUT framework assumes that your PID efficiency maps are binned in momentum, rapidity and number of tracks.
In particular, it is assumed that you have many 2-dimensional look-up tables (binned in momentum and rapidity), each of it related to a range of `nTracks`.
For more informations, please refer to the description of the makePIDEff script.
In the effLUT config file you have to specify:
- `ntracks_name`: the variable name of nTracks, as it appears in the dataset that you want to correct;
- `varxP_varyEta`: flag to specify if the x variable is the momentum and the y one is the rapidity (== 1), or if it is the opposite (== 0); 
Moreover, for each of the PID cuts you have to set:
- the particle name on which the PID cut is applied;
- the input file and histo names containing the efficiency map.
Be aware that in your dataset you must have the single four-momentum components of the particle on which the PID is applied,
with the format `<part_name>_PX`, `<part_name>_PY`, `<part_name>_PZ`, `<part_name>_PE`.

There are some optional options to compute in the proper way the uncertainties to assign to the s-weighting procedure:
- `inFitInfo_fullFit`: output file of the default s-fit, to assign a proper uncertainty to the s-weighting;
- `inFitInfo_fixedShapes`: output file of the fit performed fixing the shapes, to assign a proper uncertainty to the s-weighting;
- `candidate_name`: name of the candidate as it appears in the output of the fits, to assign a proper uncertainty to the s-weighting.

## makeBDTEff
[Here](config/BDTEff.info) there is an example of configuration file for this macro.

You have to specify:
- `bdtcut`: BDT cut value;
- `inHisto_name`: name of the input histogram containing the distribution of particles depending on the BDT response;
- `outHisto_name`: name of te output histo containing the efficiency distribution;
- `adaptive`: to use the adaptive binning (value = 1) or the TEfficiency (value = 0).