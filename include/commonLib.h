#ifndef INCLUDE_COMMONLIB_H
#define INCLUDE_COMMONLIB_H 1

/*!
 *  @file      commonLib.h
 *  @author    Alessio Piucci
 *  @brief     Common library of methods used by different macros and structures.
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

#include <TH2A.h>

//ROOT libraries
#include "TROOT.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TEfficiency.h"

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

using namespace std;
namespace pt = boost::property_tree;

//parse a string containing some environment variables
std::string ParseEnvName(std::string input_string);

//compute the binomial error, mainly for efficiency computation
double BinomialError(int passed, int tries);

//compute the error of a generic ratio
double ErrorRatio(double num, double num_err, double den, double den_err);

//check the consistency of generated and reco histos for TH2A
void CheckConsistency_adaptive(TH2A *h_gen, TH2A *h_reco,
                               unsigned int &num_fixed_bins, bool fix);

//search and fix for negative bins
template<typename T> void FixNegativeBins(T* histo, T* histo_errors, unsigned int &num_fixed_bins, bool verbose = true);

//search and fix for entry errors for a TEfficiency
template<typename T> void FixTEfficiencyEntries(T* h_pass, T* h_total,
                                                T* histo_errors_pass, T* histo_errors_total,
                                                unsigned int &num_fixed_bins);

//check that the efficiency histo was correctly filled
template<typename T> bool CheckEfficiency(T* h_eff);

//fix weights problems for a TEfficiency, TH2D
void FixTEfficiencyWeights(TH2D* h_pass, TH2D* h_total);

//retrieve and parse a set of cuts
std::string ParseCuts(pt::ptree configtree, bool denominator_eff);

//---------//
// Implementation of templated functions

template<typename T> void FixNegativeBins(T* histo, T* histo_errors, unsigned int &num_fixed_bins, bool verbose){

  int x_bin, y_bin, z_bin;

  //loop over bins
  for(int i_bin = 0; i_bin <= histo->GetNcells(); ++i_bin){

    if(histo->GetBinContent(i_bin) < 0.){

      //convert from global bin to single x, y, z bins
      histo->GetBinXYZ(i_bin, x_bin, y_bin, z_bin);

      if(verbose)
        std::cout << "WARNING: histo has negative bin. (" << x_bin << ", " << y_bin << ", " << z_bin << ") = "
                  << histo->GetBinContent(i_bin)
                  << ". Setting the content to 0." << std::endl;

      histo->SetBinContent(i_bin, 0.);

      //I cannot use the Fill(), since this function is templated for TH*D
      //and I don't know here what's the dimensionality of the histo
      histo_errors->SetBinContent(i_bin, histo_errors->GetBinContent(i_bin) + 1.);

      ++num_fixed_bins;
    }  //if(histo->GetBinContent(i_bin) < 0.)
  }  //loop over bins

  return;
}

template<typename T> void FixTEfficiencyEntries(T* h_pass, T* h_total,
                                                T* histo_errors_pass, T* histo_errors_total,
                                                unsigned int &num_fixed_bins){

  //check: pass <= total, and positive content
  int x_bin, y_bin, z_bin;

  num_fixed_bins = 0;

  //loop over bins
  for(int i_bin = 0; i_bin <= h_total->GetNcells(); ++i_bin){

    //pass > total
    if(h_pass->GetBinContent(i_bin) > h_total->GetBinContent(i_bin)){

      //convert from global bin to single x, y
      h_total->GetBinXYZ(i_bin, x_bin, y_bin, z_bin);

      std::cout << "WARNING: more passed than total events in bin (" << x_bin << ", " << y_bin << ")"
                << ", h_total = " << h_total->GetBinContent(i_bin)
                << ", h_pass = " << h_pass->GetBinContent(i_bin)
                << ". Setting total and passed content of bins to the total number of events." << std::endl;

      //fix the bin content to the total number of events
      h_pass->SetBinContent(i_bin, h_total->GetBinContent(i_bin));

      //I cannot use the Fill(), since this function is templated for TH*D
      //and I don't know here what's the dimensionality of the histo
      histo_errors_total->SetBinContent(i_bin, histo_errors_total->GetBinContent(i_bin) + 1.);
      histo_errors_pass->SetBinContent(i_bin, histo_errors_pass->GetBinContent(i_bin) + 1.);

      ++num_fixed_bins;

    }  //pass > total
  }  //loop over bins

  //check for negative bins
  FixNegativeBins(h_pass, histo_errors_pass, num_fixed_bins);
  FixNegativeBins(h_total, histo_errors_total, num_fixed_bins);

  std::cout << std::endl;

  return;
}

template<typename T> bool CheckEfficiency(T* h_eff){

  //loop over bins
  for(int i_bin = 0; i_bin <= h_eff->GetPassedHistogram()->GetNcells(); ++i_bin){

    //check that the efficiency value is positive, and smaller than 1.
    if((h_eff->GetEfficiency(i_bin) < 0.) || (h_eff->GetEfficiency(i_bin) > 1.)){
      std::cout << "CheckEfficiency:: efficiency = " << h_eff->GetEfficiency(i_bin)
                << ", i_bin = " << i_bin << std::endl;
      return false;
    }

    //check if the efficiency value is nan
    if(std::isnan(h_eff->GetEfficiency(i_bin))){
      std::cout << "CheckEfficiency:: what the hell, efficiency is nan! i_bin = " << i_bin << std::endl;
      return false;
    }

  }  //loop over bins

  return true;
}


#endif // INCLUDE_COMMONLIB_H
