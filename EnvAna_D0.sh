
#!/bin/bash

# setup the LHCb environment
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh

# setup the ROOT installation
source /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/SetupProject.sh ROOT 6.06.02

# Root > 6.08?
export NEW_ROOT=false

# setup the compilers
export CC=gcc
export CXX=g++

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/lhcb.cern.ch/lib/lcg/releases/GSL/1.10-a0511/x86_64-slc6-gcc49-opt/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/lhcb.cern.ch/lib/lcg/releases/fftw3/3.3.4-a8420/x86_64-slc6-gcc49-opt/lib/

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# set the TH2A directory
export TH2A_DIR=$DIR/2DAdaptiveBinning

echo "TH2A_DIR has been set to " $TH2A_DIR